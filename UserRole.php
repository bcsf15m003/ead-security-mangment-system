<html>
<title>  user-role   </title>
<?php 
 session_start();
?>
<?php 

 if(isset($_SESSION["user"])==false)
 {
	 header('Location:Login.php');
 }

?>
<head>

 <style>
 .modal-content {
   background-color: solid  grey;
   width: 240px;
   height: 300px;
   padding:12px;
   text-align:left;
   border: 2px solid #888;
   float:left;
    
 }
 a, button {
			padding: 7px 20px 7px 20px;
			color: white;
			background-color: #444444;
			border-color: #444444;
			font-size: 15px;
			text-decoration: none;
		}
.header
{
	
	margin-top:-31px;
	margin-left: -12px;
	width: 110%;
	height: 30%;
	font-family:Courier New;
	 background-color:black;
	font-size:12pt;
	color:white;
} 
.container {
     
width:440px;
height:330px;
padding:2px;
margin-left:20%;
	
}
label
{
	font-size:10pt; 
}
.footer {
    margin-top: 20px;
    margin-left: -12px;
    width: 110%;
	height: 20%;
   background-color:black;
   color: white;
   text-align: right;
   padding:1px;
   
}
 </style>
 <script src="..\jquery-3.2.1.min.js" type="text/javascript"></script>
<script>
var editid=null;
	$(document).ready(function(){
		
		$("#logoutbtn").click(function (){
				window.location.href = "Login.php";
			});
		
		     ShowTable();
		      
		$("#saveuserrole").click(function (){
				
			
		  
			var user=$("#cmbuser").val();
			var role=$("#cmbroles").val();
			var dataToSend = {};
			dataToSend.USER=user;
			dataToSend.ROLE=role;
			
			if(editid==null)
			{
			var dataToSend = {};
			dataToSend.USER=user;
			dataToSend.ROLE=role;
			dataToSend.action="save-user-role";
			 
			 var setting={};
		    setting.type="POST";           //agr hmny get kiya toh head may jy ga agr post hy to body may jy ga
		    setting.datatype="json";
		    setting.url="api.php";       //api.php server ka kam kr rha hy iss may hm kah rhy hy hmari request api.php server k pas jy
		    setting.data=dataToSend;      //agr may server ko data pass krna chahti hu ......task1.php client hy r api.php server hy
		    setting.success=function(result){           //api.php response create kr k hmy send kry ga 
			 
			   $("#Table").find("tr:gt(0)").remove();
				  ShowTable();
			  
			  
		                                    };
		    setting.error=function()
		     {
			  alert('error'); 
			 
		      };

		$.ajax(setting);  //yai hm http kee request create kr rhy hy
		console.log("request sent");
			}
			else if(editid!=null)
			{
				var dataToSend = {};
			dataToSend.USER=user;
			dataToSend.ROLE=role;
			 
			 dataToSend.URID=editid;
			 dataToSend.action="update-user-role";
			 
			 
			 var setting={};
		    setting.type="POST";           //agr hmny get kiya toh head may jy ga agr post hy to body may jy ga
		    setting.datatype="json";
		    setting.url="api.php";       //api.php server ka kam kr rha hy iss may hm kah rhy hy hmari request api.php server k pas jy
		    setting.data=dataToSend;      //agr may server ko data pass krna chahti hu ......task1.php client hy r api.php server hy
		    setting.success=function(result){           //api.php response create kr k hmy send kry ga 
			 
			   $("#Table").find("tr:gt(0)").remove();
				  ShowTable();
			  
			  
		                                    };
		    setting.error=function()
		     {
			  alert('error'); 
			 
		      };

		$.ajax(setting);  //yai hm http kee request create kr rhy hy
		console.log("request sent");
			 
			 
			}
			
		   
			return false;				
			});
	
		
		
	});//end of ready
			
		function ShowTable()
		{

               var dataToSend = {};
			   dataToSend.action="show-user-role";
		
		
		        var settings= {
				type: "POST",
				dataType: "json",
				url: "api.php",
				data: dataToSend,
				success: function(result){
					
					
				for(var i=0;i<result.UserRole.length;i++)
				{
						 var obj = result.UserRole[i];
						 var tr = $("<tr>");
                         var td = $("<td>").text(obj.ID);
                         tr.append(td);

                         td = $("<td>").text(obj.User);
                         tr.append(td);
                        
						 td = $("<td>").text(obj.Role);
                         tr.append(td)
						
			             var $b = $("<button>").attr('id', i+1).text("Edit");
						 $b.click(function () {
							 var urid=$(this).closest('tr').find("td:first").text();
							 EditUserRole(urid);	 
						 });

                          td = $("<td>").append($b);
                          tr.append(td);
			  
	
                          var $b = $("<button>").attr('id', i+1).text("Delete");
                          $b.click(function () {
                          var r = confirm('Do you want to remove it?');
                          if (r) {
							      var urid=$(this).closest('tr').find("td:first").text();
                                 $(this).closest('tr').remove();
								  DeleteUserRole(urid);
					             }
                                              });

                          td = $("<td>").append($b);
                          tr.append(td);
                         $("#Table").append(tr);
                }//loop ends
												
			}//success ends
		}//setting ends
		              
		
		    $.ajax(settings);
			console.log('request sent');
		


		}		
	
	   function DeleteUserRole(urid)
		{
	
	
            var dataToSend = {};
			dataToSend.USERROLEID=urid;
			dataToSend.action="DeleteUserRole";
			
			//Step-2: Create an Object to make AJAX call
			var settings= {
				type: "POST",
				dataType:"json",
				url: "api.php",
				data: dataToSend,
				success: function(result){
					alert(result);
					
				}
			};
			
			//Step-3: Make AJAX call
			$.ajax(settings);
			console.log('request sent');    
	
	
	
		}
		function EditUserRole(urid)
		{
			editid=urid;
			//alert(editid);
			var dataToSend = {};
			dataToSend.USERROLEID=urid;
			dataToSend.action="getUserRole";
			var settings= {
				type: "POST",
				dataType:"json",
				url: "api.php",
				data: dataToSend,
				success: function(result){
			    var i=0;
			    var ur = result.UserRole[i];	
				
			    $("#cmbuser").val(ur.User);
			    $("#cmbroles").val(ur.Role); 
				},
				
				error: function()
				{
					alert("error");
				}
			};
			
			//Step-3: Make AJAX call
			$.ajax(settings);
			console.log('request sent');    
		
			
			
			
			
		} 
		function resetFields() {
      $("#cmbuser").val(0);
	  $("#cmbroles").val(0);
}
</script>
</head>

<?php require('conn.php');?>
<body>

 <div style="background-color: #444444; padding: 10px;"> 
        <a href="Home.php"> Home </a>
		<a href="Users.php"> User Management </a>
		<a href="Role.php"> Role Management </a>
		<a href="Permission.php"> Permission Management </a>
		<a href="RolePermission.php"> Role-Permission Assignment </a>
		<a href="UserRole.php"> User-Role Assignment </a>
		<a href="LoginHistory.php"> Login History </a>
		<button id="logoutbtn"> Logout </button>
	</div>
	<br>
	<br>
	
<div class="container">
	 <div class="modal-content">
	  
	     <div class="header">
         <h3 style="padding:20px;margin-left:3px;"><strong> User Role Assignemnt</strong></h3>
         </div>
	     <br>
	    
	  <label>User:</label>
	 <br>
	 <select style="width:95%;border-radius: 3px;border: 2px solid grey;"    name="cmbuser" id="cmbuser">
      <option value="0">--Select--</option>
	<?php 
	$sql="SELECT * From user";
    $result=mysqli_query($conn,$sql);
    $record=mysqli_num_rows($result);
    if($record>0)
   {
	while($row=mysqli_fetch_assoc($result))
	{
		$id=$row['userid'];
		$name=$row['name'];
		echo "<option value='$id'>$name</option>";
	}
	
   }
	 
	 ?>
	 </select>
	 <br>
	 <br>
	 
     <label >Role:</label>
	 <br>
	 <select style="width:95%;border-radius: 3px;border: 2px solid grey;"    name="cmbroles" id="cmbroles">
     <option value="0">--Select--</option>
	<?php 
	$sql="SELECT * From role";
    $result=mysqli_query($conn,$sql);
    $record=mysqli_num_rows($result);
    if($record>0)
   {
	while($row=mysqli_fetch_assoc($result))
	{
		$id=$row['roleid'];
		$name=$row['name'];
		echo "<option value='$id'>$name</option>";
	}
	
   }
	?>
    </select>
	 <br>
	 <br>
        <div class="footer">
		<button type="reset" id="reset"  onclick="resetFields();"  style="width:30%; border-radius: 5px; padding:5px;border-color:grey;">Clear</button>
        <button type="submit" name="savebtn" id="saveuserrole"  style="width:30%; border-radius: 5px; padding:5px;border-color:grey;"> save </button>
		</div>
    </div>
</div>
<div style=" margin-left:45%;margin-top:-15%;">
<table  border="2"  id="Table">
            <tr>
                <th>ID</th>
                <th>User</th>
                <th>Role</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
        </table>
</div>


</body>

</html>
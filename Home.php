<!DOCTYPE html>
<html>
<head>
	<title> Home </title>

	<style type="text/css">
		a, button {
			padding: 7px 20px 7px 20px;
			color: white;
			background-color: #444444;
			border-color: #444444;
			font-size: 15px;
			text-decoration: none;
		}
	</style>
	<script>
	function Main(){
			var b = document.getElementById('lobtn');
			b.onclick = function (){
				window.location.href = "Login.php";
			}
		} // 
	</script>
</head>

<body onload="Main();">

<?php
session_start();

?>
<?php 

if($_SESSION["user"]=="admin")
{
	  echo "<div style='background-color: #444444; padding: 10px;'>";
	  echo "<a href='Home.php'> Home </a>";
	  echo "<a href='Users.php'> User Management </a>";
	  echo "<a href='Role.php'> Role Management </a>";
	  echo "<a href='Permission.php'> Permission Management </a>";
	  echo "<a href='RolePermission.php'> Role-Permission Assignment </a>";
	  echo "<a href='UserRole.php'> User-Role Assignment </a>";
	  echo "<a href='LoginHistory.php'> Login History </a>";
	  echo "<button id='lobtn'> Logout </button>";
	  echo "</div>";
	  echo "<div style='color: #333333; font-size: 50px; font-weight: bolder; margin-top: 20px;'>";
	  echo "<center>Welcome Admin<center>";
	  echo "</div>";
}
else if($_SESSION["user"]=="user")
{    
    
	  echo "<div style='background-color: #444444; padding: 10px;'>"; 
	  echo "<a href='Home.php'> Home </a>";
	  echo "<a href='users-roles-per.php'> User-Role Assignment </a>";
	  echo "<button id='lobtn'> Logout </button>";
	  echo "</div>";

	  echo "<div style='color: #333333; font-size: 50px; font-weight: bolder; margin-top: 20px;'>";
	  echo "<center>Welcome User<center>";
	  echo "</div>";

		
}
else if($_SESSION["user"]==false)
{
 
header("Location: Login.php");
}
?>

</body>
</html>
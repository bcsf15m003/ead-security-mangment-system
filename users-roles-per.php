``<html>
<title> users-roles-per</title>
<?php 
 session_start();
?>
<?php 

 if(isset($_SESSION["user"])==false)
 {
	 header('Location:Login.php');
 }

?>
<head>

 <style>
 a, button {
			padding: 7px 20px 7px 20px;
			color: white;
			background-color: #444444;
			border-color: #444444;
			font-size: 15px;
			text-decoration: none;
		}

 </style>
<script src="..\jquery-3.2.1.min.js" type="text/javascript"></script>
<script>

$(document).ready(function(){
	
	$("#logoutbtn").click(function (){
				window.location.href = "Login.php";
			});
	show_welcome_role();
});



function show_welcome_role()
		{

               var dataToSend = {};
			   dataToSend.action="show_welcome_role";
		
		      
		        var settings= {
				type: "POST",
				dataType: "json",
				url: "api.php",
				data: dataToSend,
				success: function(result){
				
				var i=0;
			
				var obj=result.User[i];	
			    var name=obj.UserName;
				var role=obj.UserRole;
				var rid=obj.RoleID;
				var msg=" Welcome   "+name;
				$("#name").text(msg);
				msg="Role:       "+role;
				$("#role").text(msg);
				
				Show_role_permission(rid);					
			}//success ends
			
		}//setting ends
		              
		
		    $.ajax(settings);
			console.log('request sent');
		


		}


function Show_role_permission(rid)
{

var dataToSend = {};
			   dataToSend.action="show_perm_for_spc_role";
		       dataToSend.RID=rid;
		      
		        var settings= {
				type: "POST",
				dataType: "json",
				url: "api.php",
				data: dataToSend,
				success: function(result){
				
			          for(var i=0;i<result.Perms.length;i++)
				{
					   
						 var obj = result.Perms[i];
						 var tr = $("<tr>");
                         var td = $("<td>").text(obj.ID);
                         tr.append(td);

                         td = $("<td>").text(obj.Name);
                         tr.append(td);
                        
						 td = $("<td>").text(obj.Description);
                         tr.append(td);
                         $("#Table").append(tr);
                }//loop ends
								
			
			    					
			}//success ends
			
		}//setting ends
		              
		
		    $.ajax(settings);
			console.log('request sent');
		


}    
</script>
</head>


<body>

 <div style="background-color: #444444; padding: 10px;"> 
		<a href="Home.php"> Home </a>
		<a href="users-roles-per.php"> Users-Roles Assignment </a>
		<button id="logoutbtn"> Logout </button>
	</div>

<center><div  id="name" style='width:40%;color: white; background-color: #444444;font-size: 50px; font-weight: bolder; margin-top: 20px;'></div></center>
<center><h1  id="role" style='color:grey;'></h1></center>

<center><table  border="2"  id="Table">
            <tr>
                <th>ID</th>
				<th>Name</th>
                <th>Permission</th>
            </tr>
			
			

</table>
</center>
</body>

</html>
<?php require('conn.php')

?>

<?php
session_start();

?>
<?php 
if(isset($_REQUEST["action"])  && !empty($_REQUEST["action"]))
{
    $action=$_REQUEST["action"];
       if($action=="showcity")
		{
  
        $country_id =$_REQUEST["CID"];		
        $cities = array();

        $index=0;
        $sql="SELECT * From city WHERE countryid='$country_id'";
        $result=mysqli_query($conn,$sql);
        $record=mysqli_num_rows($result);
        if($record>0)
        {
	      while($row=mysqli_fetch_assoc($result))
	      {
		     $id=$row['id'];
		     $name=$row['name'];
		     $cities[$index]=array("CityID" => $id, "Name" => $name);
		     $index++;
		    //echo("fir")
	      }
	
        }
        $output["Cities"] = $cities;
		
        //Return above object in JSON format
        echo json_encode($output);

       }
		else if($action=="saveuser")
		{
			
			
			$dec=1;
			
			$usr=array();
			$login =$_REQUEST["LOGIN"];
			$password =$_REQUEST["PASS"];
			$name = $_REQUEST["NAME"];
			$email = $_REQUEST["EMAIL"];
			$country = $_REQUEST["CID"];
			$city = $_REQUEST["CITYID"];
			$createdon= date("Y-m-d h:i:s");
			$isadmin=$_REQUEST["ISADMIN"];
			$createdby=	$_SESSION["userid"];
			//validation chk on uname and email
			
			
			$sql= "SELECT * FROM user";
            $result=mysqli_query($conn,$sql);
            $records=mysqli_num_rows($result);
            if($records>0)
            {
	           while($row=mysqli_fetch_assoc($result))
	           {
		         
		         $log=$row['login'];
		         $em=$row['email'];
				 if($log==$login || $em==$email)
				 {
					 $dec=0;
                     		 
					 
				 }
			   }
			}
			
			if($dec!=0)
			{
			
			$sql = "INSERT INTO user (login, password,name,email,countryid,createdon,createdby,isadmin,cityid)
				VALUES ('$login', '$password','$name','$email','$country','$createdon','$createdby','$isadmin','$city')";
			
			mysqli_query($conn, $sql); 
	
	   
	        $output=true;
			echo json_encode($output);
			
		
		    
		}
		}
		else if($action=="saverole")
		{
			$r =$_REQUEST["ROLE"];
			$des =$_REQUEST["DESCRIPTION"];
            $createdon= date("Y-m-d h:i:s");
			$createdby=	$_SESSION["userid"];
			
			$sql = "INSERT INTO role (name,description,createdon,createdby)
				VALUES ('$r', '$des','$createdon','$createdby')";
			
			if (mysqli_query($conn, $sql) === TRUE) 
			{
				$last_id = mysqli_insert_id($conn);
				$output=true;
				
			} 	
			
			 
			echo json_encode($output);
			
		
		}
		else if($action=="saveperm")
		{
			$p =$_REQUEST["PERM"];
			$des =$_REQUEST["DESCRIPTION"];
            $createdon= date("Y-m-d h:i:s");
			$createdby=	$_SESSION["userid"];
			
			$sql = "INSERT INTO permission (name,description,createdon,createdby)
				VALUES ('$p', '$des','$createdon','$createdby')";
			
			if (mysqli_query($conn, $sql) === TRUE) 
			{
				$last_id = mysqli_insert_id($conn);
				$output=true;
				
			} 	
			
			 
			echo json_encode($output);
			
		
		}
		else if($action=="saveroleperm")
		{
			$r =$_REQUEST["ROLE"];
			$p =$_REQUEST["PERMISSION"];
           
			
			$sql = "INSERT INTO role_permission (roleid,permissionid)
				VALUES ('$r', '$p')";
			
			if (mysqli_query($conn, $sql) === TRUE) 
			{
				$last_id = mysqli_insert_id($conn);
				$output=true;
				
			} 	
			
			 
			echo json_encode($output);
			
		
		}
		else if($action=="save-user-role")
		{
			$u =$_REQUEST["USER"];
			$r =$_REQUEST["ROLE"];
           
			
			$sql = "INSERT INTO user_role (userid,roleid)
				VALUES ('$u', '$r')";
			
			if (mysqli_query($conn, $sql) === TRUE) 
			{
				$last_id = mysqli_insert_id($conn);
				$output=true;
				
			} 	
			
			 
			echo json_encode($output);
			
		
		}
		else if($action=="showtable")
		{
        $users = array();

        $index=0;
        $sql="SELECT * From user";
        $result=mysqli_query($conn,$sql);
        $record=mysqli_num_rows($result);
        if($record>0)
        {
	      while($row=mysqli_fetch_assoc($result))
	      {
		     $id=$row['userid'];
		     $name=$row['name'];
			 $email=$row['email'];
		     $users[$index]=array("UserID" => $id, "Name" => $name,"Email" => $email);
		     $index++;
		   
	      }
	
        }
        $output["Users"] = $users;
		
        //Return above object in JSON format
        echo json_encode($output);
       

        }
		else if($action=="showrole")
		{
        $roles = array();

        $index=0;
        $sql="SELECT * From role";
        $result=mysqli_query($conn,$sql);
        $record=mysqli_num_rows($result);
        if($record>0)
        {
	      while($row=mysqli_fetch_assoc($result))
	      {
		     $id=$row['roleid'];
		     $name=$row['name'];
			 $des=$row['description'];
		     $roles[$index]=array("RoleID" => $id, "RoleName" => $name,"RoleDes" => $des);
		     $index++;
		   
	      }
	
        }
        $output["Roles"] = $roles;
		
        //Return above object in JSON format
        echo json_encode($output);
       

        }
		else if($action=="showperm")
		{
        $perms = array();

        $index=0;
        $sql="SELECT * FROM permission";
        $result=mysqli_query($conn,$sql);
        $record=mysqli_num_rows($result);
        if($record>0)
        {
	      while($row=mysqli_fetch_assoc($result))
	      {
		     $id=$row['permissionid'];
		     $name=$row['name'];
			 $des=$row['description'];
		     $perms[$index]=array("PermID" => $id, "PermName" => $name,"PermDes" => $des);
		     $index++;
		   
	      }
	
        }
        $output["Perms"] = $perms;
		
        //Return above object in JSON format
        echo json_encode($output);
       

        }
		else if($action=="showroleperm")
		{
        $roleperms = array();

        $index=0;
        $sql="SELECT * FROM role_permission";
        $result=mysqli_query($conn,$sql);
        $record=mysqli_num_rows($result);
        if($record>0)
        {
	      while($row=mysqli_fetch_assoc($result))
	      {
		     $id=$row['id'];
		     $rid=$row['roleid'];
			 $pid=$row['permissionid'];
			  
			 $sql2="SELECT * FROM role WHERE roleid='$rid'";
			 $res=mysqli_query($conn,$sql2);
			 $row2=mysqli_fetch_assoc($res);
			 $role=$row2['name'];
			 
			 
			 
			 $sql3="SELECT * FROM permission WHERE permissionid='$pid'";
			 $res3=mysqli_query($conn,$sql3);
			 $row3=mysqli_fetch_assoc($res3);
			 $perm=$row3['name'];
			 
		     $roleperms[$index]=array("RolePermID" => $id, "Role" => $role,"Perm" => $perm);
		     $index++;
		   
	      }
	
        }
        $output["RolesPerms"] = $roleperms;
		
        //Return above object in JSON format
        echo json_encode($output);
       

        }
		else if($action=="show-user-role")
		{
        $userrole = array();

        $index=0;
        $sql="SELECT * FROM user_role";
        $result=mysqli_query($conn,$sql);
        $record=mysqli_num_rows($result);
        if($record>0)
        {
	      while($row=mysqli_fetch_assoc($result))
	      {
		     $id=$row['id'];
		     $uid=$row['userid'];
			 $rid=$row['roleid'];
			  
			 $sql2="SELECT * FROM user WHERE userid='$uid'";
			 $res=mysqli_query($conn,$sql2);
			 $row2=mysqli_fetch_assoc($res);
			 $user=$row2['name'];
			 
			 
			 
			 $sql3="SELECT * FROM role WHERE roleid='$rid'";
			 $res3=mysqli_query($conn,$sql3);
			 $row3=mysqli_fetch_assoc($res3);
			 $role=$row3['name'];
			 
		     $userrole[$index]=array("ID" => $id, "User" => $user,"Role" => $role);
		     $index++;
		   
	      }
	
        }
        $output["UserRole"] = $userrole;
		
        //Return above object in JSON format
        echo json_encode($output);
       

        }
		else if($action=="show-login-history")
		{
        $history = array();

        $index=0;
        $sql="SELECT * FROM loginhistory";
        $result=mysqli_query($conn,$sql);
        $record=mysqli_num_rows($result);
        if($record>0)
        {
	      while($row=mysqli_fetch_assoc($result))
	      {
		     $id=$row['id'];
		     $uid=$row['userid'];
			 $l=$row['login'];
			 $lt=$row['logintime'];
			 $mip=$row['machineip'];
			 
		     $history[$index]=array("ID" => $id, "UserID" => $uid,"Login" => $l,"LoginTime" => $lt,"MachineIP" => $mip);
		     $index++;
		   
	      }
	
        }
        $output["LoginHistory"] = $history;
		
        //Return above object in JSON format
        echo json_encode($output);
       

        }
		else if($action=="show_welcome_role")
		{
        $user = array();

		$uid=$_SESSION["userid"];

        $sql= "SELECT * FROM user WHERE userid=$uid";
        $result=mysqli_query($conn,$sql);
        $row=mysqli_fetch_assoc($result);
        $uname=$row['name'];
		
		$sql= "SELECT * FROM user_role WHERE userid=$uid";
        $result=mysqli_query($conn,$sql);
        $row=mysqli_fetch_assoc($result);
        $rid=$row['roleid'];

        $sql= "SELECT * FROM role WHERE roleid=$rid";
        $result=mysqli_query($conn,$sql);
        $row=mysqli_fetch_assoc($result);
        $rolename=$row['name'];
		
		$index=0;
		$user[$index]=array("UserName" => $uname, "RoleID"=>$rid,"UserRole" => $rolename);
		$index++;
		   
	   
        $output["User"] = $user;
		
        //Return above object in JSON format
        echo json_encode($output);
       

        }
		
		else if($action=="show_perm_for_spc_role")
		{
        $perms = array();
        $index=0;
		
		$id=$_REQUEST["RID"];
		
		
		
		$sql= "SELECT * FROM role_permission WHERE roleid=$id";
        $result=mysqli_query($conn,$sql);
        $records=mysqli_num_rows($result);
       if($records >0)
        {
	        while($row=mysqli_fetch_assoc($result))
	        {
               $pid=$row['permissionid'];
	            
				$sql= "SELECT * FROM permission WHERE permissionid=$pid";
                $res=mysqli_query($conn,$sql);
	            $r=mysqli_fetch_assoc($res);
	            
				$name=$r['name'];
	            $des=$r['description'];
	            $perms[$index]=array("ID" => $pid,"Name" => $name, "Description" => $des);
				$index++;
                 
	 
	        }


        }
		
		$output["Perms"]=$perms;
		 echo json_encode($output);
       
     }
		else if($action=="DeleteRole")
		{

	     $id=$_REQUEST['ROLEID'];
        $q="Delete FROM role WHERE roleid=$id";
        $reults=mysqli_query($conn,$q);
		$output=true;
        echo json_encode($output);
       

        }
		else if($action=="DeleteRolePerm")
		{

	    $id=$_REQUEST['ROLEPERMID'];
        $q="Delete FROM role_permission WHERE id=$id";
        $reults=mysqli_query($conn,$q);
		$output=true;
        echo json_encode($output);
       

        }
		else if($action=="DeleteUserRole")
		{

	    $id=$_REQUEST['USERROLEID'];
        $q="Delete FROM user_role WHERE id=$id";
        $reults=mysqli_query($conn,$q);
		$output=true;
        echo json_encode($output);
       

        }
		else if($action=="DeletePerm")
		{

	    $id=$_REQUEST['PERMID'];
        $q="Delete FROM permission WHERE permissionid=$id";
        $reults=mysqli_query($conn,$q);
		$output=true;
        echo json_encode($output);
       

        }
		else if($action=="DeleteUser")
		{

	     $id=$_REQUEST['USERID'];
        $q="Delete FROM user WHERE userid=$id";
        $reults=mysqli_query($conn,$q);
		$output=true;
        echo json_encode($output);
       

        }
		else if($action=="getUser")
	{

	    $id=$_REQUEST['USERID'];
        $users = array();

        
        $sql="SELECT * From user WHERE userid=$id";
        $result=mysqli_query($conn,$sql);
        $row=mysqli_fetch_assoc($result);
		     
		$index=0;	 
		$Login=$row['login'];
		$Pass=$row['password'];
		$Name=$row['name'];
	    $Email=$row['email'];	
		$CID=$row['countryid'];
		$CityId=$row['cityid'];
		$IsAdmin=$row['isadmin'];
		$users[$index]=array("USERLOGIN" => $Login, "PASS" => $Pass,"NAME" => $Name,"EMAIL" => $Email, "CONID" => $CID,"CITYID" => $CityId, "ISADMIN"=>$IsAdmin);
		     
		  $output["Users"]=$users;
        //Return above object in JSON format
        echo json_encode($output);

        }
		else if($action=="getRole")
	{

	    $id=$_REQUEST['ROLEID'];
        $roles = array();

        
        $sql="SELECT * From role WHERE roleid=$id";
        $result=mysqli_query($conn,$sql);
        $row=mysqli_fetch_assoc($result);
		     
		$index=0;	 
		  $id=$row['roleid'];
		 $name=$row['name'];
	     $des=$row['description'];
		$roles[$index]=array("RoleID" => $id, "RoleName" => $name,"RoleDes" => $des);
		     
		  $output["Roles"]=$roles;
        //Return above object in JSON format
        echo json_encode($output);

        }
		else if($action=="getRolePerm")
	{

	    $id=$_REQUEST['ROLEPERMID'];
        $rolesperms = array();
       
        
        $sql="SELECT * From role_permission WHERE id='$id'";
        $result=mysqli_query($conn,$sql);
        $row=mysqli_fetch_assoc($result);
		     
		$index=0;	 
		  $id=$row['id'];
		 $role=$row['roleid'];
	     $perm=$row['permissionid'];
		$rolesperms[$index]=array("ID" => $id, "Role" => $role,"Perm" => $perm);
		     
		  $output["RolesPerms"]=$rolesperms;
        //Return above object in JSON format
        echo json_encode($output);

        }
		else if($action=="getUserRole")
	{

	    $id=$_REQUEST['USERROLEID'];
        $userrole = array();
       
        
        $sql="SELECT * From user_role WHERE id='$id'";
        $result=mysqli_query($conn,$sql);
        $row=mysqli_fetch_assoc($result);
		     
		$index=0;	 
		  $id=$row['id'];
		 $user=$row['userid'];
	     $role=$row['roleid'];
		$userrole[$index]=array("ID" => $id, "User" => $user,"Role" => $role);
		     
		  $output["UserRole"]=$userrole;
        //Return above object in JSON format
        echo json_encode($output);

        }
		else if($action=="getPerm")
	    {

	    $id=$_REQUEST['PERMID'];
        $perms = array();

        
        $sql="SELECT * From permission WHERE permissionid=$id";
        $result=mysqli_query($conn,$sql);
        $row=mysqli_fetch_assoc($result);
		     
		$index=0;	 
		  $id=$row['permissionid'];
		 $name=$row['name'];
	     $des=$row['description'];
		$perms[$index]=array("PermID" => $id, "PermName" => $name,"PermDes" => $des);
		     
		  $output["Perms"]=$perms;
        //Return above object in JSON format
        echo json_encode($output);

        }
		
		
		
		
		
        else if($action=="updateuser")
	{

	    $id=$_REQUEST['UID'];
		
		
		$login =$_REQUEST["LOGIN"];
		$password =$_REQUEST["PASS"];
		$name = $_REQUEST["NAME"];
		$email = $_REQUEST["EMAIL"];
		$country = $_REQUEST["CID"];
		$city = $_REQUEST["CITYID"];
		$isadmin=$_REQUEST["ISADMIN"];
		
		
		
        $sql = "UPDATE user SET login='$login',password='$password',name='$name',email='$email',countryid='$country',cityid='$city',isadmin='$isadmin' WHERE userid='$id'";
			
			if (mysqli_query($conn, $sql) === TRUE) 
			{
				$output=true;
			}
			else
			{
				$output=false;
			}
				
        echo json_encode($output);

        }
		
		  else if($action=="updaterole")
	{

	    $id=$_REQUEST['RID'];
		
		
		$r =$_REQUEST["ROLE"];
		$d =$_REQUEST["DESCRIPTION"];
		
		
		
		
        $sql = "UPDATE role SET name='$r',description='$d' WHERE roleid='$id'";
			
			if (mysqli_query($conn, $sql) === TRUE) 
			{
				$output=true;
			}
			else
			{
				$output=false;
			}
				
        echo json_encode($output);

        }
		  else if($action=="updateperm")
	{

	    $id=$_REQUEST['PID'];
		
		
		$p =$_REQUEST["PERM"];
		$d =$_REQUEST["DESCRIPTION"];
		
		
		
		
        $sql = "UPDATE permission SET name='$p',description='$d' WHERE permissionid='$id'";
			
			if (mysqli_query($conn, $sql) === TRUE) 
			{
				$output=true;
			}
			else
			{
				$output=false;
			}
				
        echo json_encode($output);

        }
		else if($action=="updateroleperm")
	   {

	    $id=$_REQUEST['RPID'];
		
		
		$r =$_REQUEST["ROLE"];
		$p =$_REQUEST["PERMISSION"];
		
		
		
		
        $sql = "UPDATE role_permission SET roleid='$r',permissionid='$p' WHERE id='$id'";
			
			if (mysqli_query($conn, $sql) === TRUE) 
			{
				$output=true;
			}
			else
			{
				$output=false;
			}
				
        echo json_encode($output);

        }
		else if($action=="update-user-role")
	   {

	    $id=$_REQUEST['URID'];
		
		
		$u =$_REQUEST["USER"];
		$r =$_REQUEST["ROLE"];
		
		
		
		
        $sql = "UPDATE user_role SET userid='$u',roleid='$r' WHERE id='$id'";
			
			if (mysqli_query($conn, $sql) === TRUE) 
			{
				$output=true;
			}
			else
			{
				$output=false;
			}
				
        echo json_encode($output);

        }

		

		
		}//end of check that checks action is not empty or null 

?>
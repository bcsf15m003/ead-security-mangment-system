<html>
<title>   login   </title>
<head>

 <style>
 .modal-content {
   background-color: solid  grey;
   width: 300px;
   height: 300px;
   padding:12px;
   text-align:left;
   border: 2px solid #888;
   float:left;
    
 }
.header
{
	
	margin-top:-33px;
	margin-left: -12px;
	width: 108%;
	height: 20%;
	font-family:Courier New;
	 background-color:black;
	font-size:12pt;
	color:white;
} 
.container {
     padding: 2px;
	
}
label
{
	font-size:14pt; 
}
.footer {
    margin-top: 42px;
    margin-left: -12px;
    width: 108%;
	height: 20%;
   background-color:black;
   color: white;
   text-align: right;
   
}
.whole-container
{


width:440px;
height:300px;
padding:2px;
margin:auto;


}
 </style>
 <script type="text/javascript">
	function Main(){
		var b1 = document.getElementById('btn1');
		b1.onclick = function(){
			
			var uname=document.getElementById("usrname").value;	
			var pass=document.getElementById("pass").value;
			if(pass.length==0 && uname.length==0)
			{
				alert("username and password both field are empty");
				return false;
			}
			else if(uname.length==0)
			{
				alert("username is empty");
				return false;
			}
			else if(pass.length==0)
			{
				alert("password is empty");
				return false;
			}
			
			return true; 
		};		
	}
</script>
<?php require('conn.php');?>
<?php
session_start();
$flag=0;

$error="";
$_SESSION["user"]=null;
?>
<?php 

$ip="";
if(getenv('HTTP_CLIENT_IP'))
{
  $ip=getenv('HTTP_CLIENT_IP');
}
else if(getenv('HTTP_X_FORWARDED_FOR'))
{
	$ip=getenv('HTTP_X_FORWARDED_FOR');
}
else if(getenv('HTTP_X_FORWARDED'))
{
	$ip=getenv('HTTP_X_FORWARDED');
}
else if(getenv('HTTP_FORWARDED_FOR'))
{
	$ip=getenv('HTTP_FORWARDED_FOR');
	
}
elseif(getenv('HTTP_FORWARDED'))
{
	$ip=getenv('HTTP_FORWARDED');
	
}
else if(getenv('REMOTE_ADDR'))
{
	$ip=getenv('REMOTE_ADDR');
	
}
else
{
	$ip='UNKNOWN';
}


if(isset($_REQUEST["btn1"]) == true)
	{
		$uname = $_REQUEST["usrname"];   
		$pswd = $_REQUEST["pass"];
		
		
		$sql="SELECT userid,login,password,isadmin FROM user";
  
       $result=mysqli_query($conn,$sql);
       $records_found=mysqli_num_rows($result);
 
      if($records_found>0)
        {
	         while($row=mysqli_fetch_assoc($result))
	         {
				$id=  $row['userid'];
	            $login= $row['login'];
	            $pass= $row['password'];
	            $isadmin=$row['isadmin'];
				
				if($uname == $login && $pswd == $pass && $isadmin==1)
		        {
				 $flag=1;
		        $_SESSION["user"]="admin";
				$_SESSION["userid"]=$id;
				$logintime= date("Y-m-d h:i:s");
				$sql = "INSERT INTO loginhistory (userid, login,logintime,machineip)
				VALUES ('$id', '$login','$logintime','$ip')";
				mysqli_query($conn,$sql);
			    header('Location:Home.php'); //To redirect to another page
				
		        }
				else if($uname == $login && $pswd == $pass && $isadmin==0)
				{
				    $flag=1;
					$_SESSION["user"]="user";
					$_SESSION["userid"]=$id;
					$logintime= date("Y-m-d h:i:s");
				   $sql = "INSERT INTO loginhistory (userid, login,logintime,machineip)
				   VALUES ('$id', '$login','$logintime','$ip')";
					mysqli_query($conn,$sql);
					
					header('Location:Home.php');
					
				}
				
	         }
	 
	 
        }
		if($flag==0)
		{
			$error="Invalid username and password";
		}
		 
	
		
	
	}
?>

</head>
<span style="font-size:14pt;text-align:left;color:red"><?php echo $error;?></span>
<body onload="Main();">
<div class="whole-container">
 <strong><h1 style="margin-left:-1px;color:black;font-size:30pt;font-family:Courier New">Security Manager</h1></strong>
<form action="" method="GET">
 <div class="container">
	 <div class="modal-content">
	  
	     <div class="header">
         <h3 style="font-size:16pt;padding:5px;margin-left:120px;"><strong> Login </strong></h3>
         </div>
	     <br>
	 
     <label ><strong>Username:</strong></label>
	 <br>
	 <input size=40 style="height:10%;border-radius: 3px;border: 2px solid grey;" type="text"  name="usrname" id="usrname" required>
	 <br>
	 <br>
	 <label><strong>Password:</strong></label>
	 <br>
	 <input size=40 style="height:10%;border-radius: 3px;border:2px solid grey;" type="password"  name="pass" id="pass" required>
	 <br>
	 <br>
        <div class="footer">
        <button type="submit"  name="btn1" id="btn1"  style="font-size:12pt;height:60%;width:30%;border-radius: 5px; padding:5px;border-color:grey;"><strong> Login </strong></button>
		</div>
    </div>
</div>
</form>
</div>


</body>

</html>
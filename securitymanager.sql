-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 31, 2018 at 01:38 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `securitymanager`
--

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `countryid` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `name`, `countryid`) VALUES
(1, 'Lahore', 1),
(2, 'queeta', 1),
(3, 'Delhi', 2),
(4, 'shikago', 3);

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id`, `name`) VALUES
(1, 'Pakistan'),
(2, 'India'),
(3, 'China');

-- --------------------------------------------------------

--
-- Table structure for table `loginhistory`
--

CREATE TABLE `loginhistory` (
  `id` int(11) NOT NULL,
  `userid` int(11) DEFAULT NULL,
  `login` varchar(50) DEFAULT NULL,
  `logintime` datetime DEFAULT NULL,
  `machineip` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `loginhistory`
--

INSERT INTO `loginhistory` (`id`, `userid`, `login`, `logintime`, `machineip`) VALUES
(1, 1, 'faryal', '2018-03-25 11:18:53', '::1'),
(4, 1, 'faryal', '2018-03-25 11:45:09', '::1'),
(5, 2, 'arfa', '2018-03-25 11:45:33', '::1'),
(6, 1, 'faryal', '2018-03-25 12:05:06', '::1'),
(7, 1, 'faryal', '2018-03-25 12:16:55', '::1'),
(8, 2, 'arfa', '2018-03-25 12:17:45', '::1'),
(9, 1, 'faryal', '2018-03-25 12:37:28', '::1'),
(10, 2, 'arfa', '2018-03-25 12:59:25', '::1'),
(11, 1, 'faryal', '2018-03-25 01:03:06', '::1'),
(12, 2, 'arfa', '2018-03-25 01:04:43', '::1'),
(13, 1, 'faryal', '2018-03-25 01:10:35', '::1'),
(14, 6, 'maryam', '2018-03-25 01:11:15', '::1'),
(15, 1, 'faryal', '2018-03-25 01:11:44', '::1'),
(16, 6, 'maryam', '2018-03-25 01:12:21', '::1'),
(17, 2, 'arfa', '2018-03-25 01:31:26', '::1'),
(18, 1, 'faryal', '2018-03-25 04:19:47', '::1'),
(19, 6, 'maryam', '2018-03-25 04:24:03', '::1'),
(20, 1, 'faryal', '2018-03-26 10:07:30', '::1'),
(21, 2, 'arfa', '2018-03-26 10:08:21', '::1'),
(22, 1, 'faryal', '2018-03-26 10:10:14', '::1');

-- --------------------------------------------------------

--
-- Table structure for table `permission`
--

CREATE TABLE `permission` (
  `permissionid` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL,
  `createdon` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `permission`
--

INSERT INTO `permission` (`permissionid`, `name`, `description`, `createdon`, `createdby`) VALUES
(1, 'View', ' view result.', '2018-03-23 04:02:11', 1),
(2, 'Upload Reasult', 'upload result. ', '2018-03-24 02:36:20', 1),
(9, 'Teacher evaluation', 'Teaching performance evaluated.', '2018-03-25 12:07:32', 1);

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `roleid` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL,
  `createdon` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`roleid`, `name`, `description`, `createdon`, `createdby`) VALUES
(1, 'Teacher', 'Teacher teaches students.', '2018-03-23 03:54:58', 1),
(2, 'Student', 'Students do study.', '2018-03-24 01:26:09', 1),
(12, 'Admin', 'manages whole system.', '2018-03-25 12:27:12', 1);

-- --------------------------------------------------------

--
-- Table structure for table `role_permission`
--

CREATE TABLE `role_permission` (
  `id` int(11) NOT NULL,
  `roleid` int(11) DEFAULT NULL,
  `permissionid` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role_permission`
--

INSERT INTO `role_permission` (`id`, `roleid`, `permissionid`) VALUES
(1, 1, 1),
(4, 2, 1),
(6, 1, 2),
(8, 12, 1),
(9, 2, 9);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `userid` int(11) NOT NULL,
  `login` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `countryid` int(11) DEFAULT NULL,
  `createdon` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `isadmin` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`userid`, `login`, `password`, `name`, `email`, `countryid`, `createdon`, `createdby`, `isadmin`) VALUES
(1, 'faryal', '456', 'Faryal Anjum', 'faryal121@gmail.com', 1, '2018-03-24 02:04:00', 0, 1),
(2, 'arfa', '456', 'Arfa nasir', 'arfa121@gmail.com', 2, '2018-03-24 10:59:09', 0, 0),
(5, 'humna', '123', 'Humna Aleem', 'humna@gmail.com', 1, '2018-03-24 09:56:35', 0, 0),
(6, 'maryam', '456', 'Maryam Umer', 'maryam14@gmail.com', 3, '2018-03-26 10:12:33', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_role`
--

CREATE TABLE `user_role` (
  `id` int(11) NOT NULL,
  `userid` int(11) DEFAULT NULL,
  `roleid` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_role`
--

INSERT INTO `user_role` (`id`, `userid`, `roleid`) VALUES
(1, 2, 1),
(3, 6, 2),
(4, 1, 12),
(9, 5, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `loginhistory`
--
ALTER TABLE `loginhistory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permission`
--
ALTER TABLE `permission`
  ADD PRIMARY KEY (`permissionid`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`roleid`);

--
-- Indexes for table `role_permission`
--
ALTER TABLE `role_permission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`userid`);

--
-- Indexes for table `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `loginhistory`
--
ALTER TABLE `loginhistory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `permission`
--
ALTER TABLE `permission`
  MODIFY `permissionid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `roleid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `role_permission`
--
ALTER TABLE `role_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `userid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `user_role`
--
ALTER TABLE `user_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
